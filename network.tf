#############################################

#Creating Virtual Private Cloud:

#############################################
resource "aws_vpc" "vpc" {
  cidr_block           = "${var.vpc-cidr-block}"
  enable_dns_hostnames = true
  enable_dns_support   = true
  

  tags = {
    Name = "${var.vpc-tag-name}"
  }
}

#############################################

#Creating Internet Gateway

#############################################
resource "aws_internet_gateway" "ig" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name = "${var.ig-tag-name}"
  }
}

resource "aws_subnet" "public_subnet" {
  count = var.nb_instance
  vpc_id     = "${aws_vpc.vpc.id}"
  #cidr_block = "${var.subnet-cidr-block}"
  availability_zone = data.aws_availability_zones.azs.names[count.index]
  cidr_block        = element(cidrsubnets(var.custom_vpc, 8, 4, 4), count.index)
  tags = {
    Name = "${var.subnet-tag-name}-${count.index}"
  }
}

#############################################

# Creating Public Route Table:

#############################################
resource "aws_route_table" "rt" {
  vpc_id = "${aws_vpc.vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.ig.id}"
  }
}

#############################################

# Creating Public Route Table Association:

#############################################
resource "aws_route_table_association" "rta" {
  count = var.nb_instance  
  #subnet_id      = "${aws_subnet.public_subnet.id}"
  subnet_id      = element(aws_subnet.public_subnet.*.id, count.index)
  route_table_id = "${aws_route_table.rt.id}"
}


#############################################

# Creating VPC FlowLogs:

#############################################

# resource "aws_flow_log" "vpc_flow_log" {
#   iam_role_arn         = data.aws_iam_role.iam_role.arn
#   log_destination_type = "cloud-watch-logs"
#   log_destination      = aws_cloudwatch_log_group.cloudwatch_log_group.arn
#   traffic_type         = "ALL"
#   vpc_id               = "${aws_vpc.vpc.id}"
# }

# ##############################################

# # Creating CloudWatch Log Group:

# ##############################################

# resource "aws_cloudwatch_log_group" "cloudwatch_log_group" {
#   name              = "VPC-FlowLogs-Group"
#   retention_in_days = 30
# }

